import ApiClient from '../infrastructure/ApiClient';

type SubmitResult = {
  status: number;
  message?: string;
};

type SubmitParams = {
  password: string;
  repassword: string;
  hintText?: string;
};

class PasswordManagerService {
  private apiClient: ApiClient;

  constructor() {
    this.apiClient = new ApiClient();
  }

  submit(params: SubmitParams): Promise<SubmitResult> {
    return this.apiClient.persist(params);
  }
}

export default PasswordManagerService;
