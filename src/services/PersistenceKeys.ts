export enum PersistenceKeys {
    PasswordManagerModalVisible = 'passwordManagerModalVisible',
    PasswordManagerCurrentStep = 'passwordManagerCurrentStep',
    PasswordManagerSuccess = 'passwordManagerSubmitSuccess',
    Password = 'password',
    PasswordCheck = 'passwordCheck',
    PasswordError = 'passwordError',
    HintText = 'hintText',
}