import Storage from './../infrastructure/Storage';

class PersistenceService {


  public get(key: string): string | null {
    return Storage.get(key);
  }

  public set(key: string, value: any): void {
    Storage.set(key, value);
  }

  public remove(key: string): void {
    Storage.remove(key);
  }

  public clear(): void {
    Storage.clear();
  }
}

export default PersistenceService;
