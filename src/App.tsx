import { FC } from 'react';
import PasswordManagerModal from './views/PasswordManagerModal/PasswordManagerModal';
import useToggleProductManagerModal from './hooks/useProductManagerModal';
import './App.css';
import Button from './components/atoms/Button/Button';
import { ButtonVariant } from './components/atoms/Button/ButtonVariant';
import { useTranslation } from 'react-i18next';

const App: FC = () => {
  const { handlerToogleModal } = useToggleProductManagerModal();
  const { t } = useTranslation();

  return (
    <div className="App">
      <header className="header"></header>
      <Button
        onClick={handlerToogleModal}
        text={t('home.password_manager_button')}
        variant={ButtonVariant.SECONDARY}
        dataCy="password_manager_button"
      />

      <PasswordManagerModal />
    </div>
  );
};

export default App;
