type UseCurrentProgressParams = {
  steps: number;
  currentStep: number;
};

export type UseCurrentProgressResult = { active: boolean; success: boolean };

const useCurrentProgress = ({
  steps,
  currentStep,
}: UseCurrentProgressParams): Record<number, UseCurrentProgressResult> => {
  const stepsArray = [...Array(steps)];

  return stepsArray.reduce(
    (acc, _, index) => ({
      ...acc,
      [index + 1]: {
        active: currentStep === index + 1,
        success: currentStep > index + 1,
      },
    }),
    {}
  );
};

export default useCurrentProgress;
