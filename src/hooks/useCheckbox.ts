import { useState } from "react";


const useCheckbox = () => {
    const [checkboxStatus, setCheckboxStatus] = useState<boolean>(false);

    const handleCheckboxStatus = () => {
        setCheckboxStatus((checkbox) => !checkbox);
    };

    return {
        checkboxStatus,
        handleCheckboxStatus,
    };
}

export default useCheckbox;