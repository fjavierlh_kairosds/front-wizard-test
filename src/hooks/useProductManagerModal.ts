import { PersistenceKeys } from './../services/PersistenceKeys';
import { useState, useMemo, useEffect } from 'react';
import PersistenceService from '../services/PersistenceService';

const useToggleProductManagerModal = () => {
  const persistenceService = useMemo(() => new PersistenceService(), []);
  const visibleState = persistenceService.get(
    PersistenceKeys.PasswordManagerModalVisible
  );

  const [modalVisible, setModalVisible] = useState<boolean>(
    JSON.parse(visibleState) || false
  );
  const handlerToogleModal = () => {
    const modalVisiblePersisted = persistenceService.get(
      PersistenceKeys.PasswordManagerModalVisible
    );

    persistenceService.set(
      PersistenceKeys.PasswordManagerModalVisible,
      !modalVisiblePersisted
    );
    setModalVisible((visible) => !visible);
  };

  useEffect(() => {
    setModalVisible(JSON.parse(visibleState));
  }, [visibleState]);

  return {
    modalVisible,
    handlerToogleModal,
  };
};

export default useToggleProductManagerModal;
