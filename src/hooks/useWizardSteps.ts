import { useEffect, useMemo, useState } from 'react';
import { PersistenceKeys } from '../services/PersistenceKeys';
import PersistenceService from '../services/PersistenceService';

type UseWizardStepsParams = {
  acceptPolicies: boolean;
  passwordError: boolean;
  password: string;
  repassword: string;
  wizardSteps: JSX.Element[];
  onSumbit: () => void;
};

const useWizardSteps = ({
  acceptPolicies,
  passwordError,
  password,
  repassword,
  wizardSteps,
  onSumbit,
}: UseWizardStepsParams) => {
  const persistenceService = useMemo(() => new PersistenceService(), []);
  const storedCurrentStep = persistenceService.get(
    PersistenceKeys.PasswordManagerCurrentStep
  );

  const [disabledNextStep, setDisabledNextStep] = useState<boolean>(true);
  const [currentStep, setCurrentStep] = useState<number>(
    JSON.parse(storedCurrentStep) || 1
  );

  const handlerSetCurrentStep = (step: number) => {
    setCurrentStep(step);
    persistenceService.set(PersistenceKeys.PasswordManagerCurrentStep, step);
  };

  const handlerNextStep = () => {
    persistenceService.set(
      PersistenceKeys.PasswordManagerCurrentStep,
      currentStep + 1
    );

    if (currentStep === 2) {
      onSumbit();
    }

    setCurrentStep((step) => {
      return step <= wizardSteps.length && ++step;
    });
  };

  useEffect(() => {
    if (currentStep === 1 && acceptPolicies) {
      setDisabledNextStep(false);
      return;
    }

    if (
      currentStep === 2 &&
      !passwordError &&
      password &&
      repassword &&
      password === repassword
    ) {
      setDisabledNextStep(false);
      return;
    }
    setDisabledNextStep(true);
  }, [acceptPolicies, currentStep, password, repassword, passwordError]);

  return {
    currentStep,
    disabledNextStep,
    handlerSetCurrentStep,
    setDisabledNextStep,
    handlerNextStep,
  };
};

export default useWizardSteps;
