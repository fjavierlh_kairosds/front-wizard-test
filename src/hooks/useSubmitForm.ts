import { useMemo, useState } from 'react';
import PasswordManagerService from '../services/PasswordManagerService';

type UseSubmitFormResult = {
  result: {
    success: boolean | undefined;
    error: string;
  };
  handlerSubmit: () => void;
  handlerResetSubmitFormResult: () => void;
};

type UseFormParams = {
  password: string;
  repassword: string;
  hintText?: string;
};

const useSubmitForm = ({
  password,
  repassword,
  hintText,
}: UseFormParams): UseSubmitFormResult => {
  const [result, setResult] = useState<{
    success: boolean;
    error: string;
  }>({
    success: undefined,
    error: '',
  });

  const productManagerService = useMemo(() => new PasswordManagerService(), []);

  const handlerSubmit = async () => {
    try {
      const { status } = await productManagerService.submit({
        password,
        repassword,
        hintText,
      });
      console.log('status', status);
      if (status === 401) {
        throw new Error('Invalid credentials');
      }
      setResult({ success: true, error: '' });
    } catch (error) {
      console.error(error);
      setResult({ success: false, error: error.message });
    }
  };

  const handlerResetSubmitFormResult = () => {
    setResult({ success: undefined, error: '' });
  };
  return { result, handlerSubmit, handlerResetSubmitFormResult };
};

export default useSubmitForm;
