import { useState } from 'react';

const useForm = () => {
  const [password, setPassword] = useState('');
  const [repassword, setRepassword] = useState('');
  const [passwordError, setPasswordError] = useState(false);
  const [hintText, setHintText] = useState('');

  return {
    password,
    setPassword,
    repassword,
    setRepassword,
    passwordError,
    hintText,
    setPasswordError,
    setHintText,
  };
};

export default useForm;
