class Storage {
  public static get(key: string): string | null {
    const storedItem = localStorage.getItem(key);

    if (!storedItem) return null;

    const item = JSON.parse(storedItem);
    const now = new Date();

    if (now.getTime() > item.expires) {
      localStorage.removeItem(key);
      return null;
    }

    return item.value;
  }

  public static set(key: string, value: any): void {
    const now = new Date();

    const item = {
      value,
      expires: now.getTime() + 60 * 60 * 1000, // minutes * seconds * milliseconds (1h)
    };
    localStorage.setItem(key, JSON.stringify(item));
  }

  public static remove(key: string): void {
    localStorage.removeItem(key);
  }

  public static clear(): void {
    localStorage.clear();
  }
}

export default Storage;
