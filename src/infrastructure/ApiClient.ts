const PRUEBA_KO = 'pruebaKO123';

const RESPONSE_OK = { status: 200 };
const RESPONSE_KO = { status: 401 };

type SubmitFormResponse = {
  status: number;
};

type SubmitFormRequest = {
  password: string;
  repassword: string;
  hintText?: string;
};

class ApiClient {
  public persist({ password }: SubmitFormRequest): Promise<SubmitFormResponse> {
    return new Promise((resolve, reject) =>
      setTimeout(
        () =>
          password !== PRUEBA_KO ? resolve(RESPONSE_OK) : reject(RESPONSE_KO),
        3000
      )
    );
  }
}

export default ApiClient;
