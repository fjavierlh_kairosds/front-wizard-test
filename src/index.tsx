import * as React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import App from './App';
import './infrastructure/i18n/i18n';
import Spinner from './components/atoms/Spinner/Spinner';

const container = document.getElementById('root');
const root = createRoot(container);
root.render(
  <React.StrictMode>
      <React.Suspense fallback={<Spinner />}>
        <App />
      </React.Suspense>
  </React.StrictMode>
);
