import classNames from 'classnames';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '../../components/atoms/Button/Button';
import { ButtonVariant } from '../../components/atoms/Button/ButtonVariant';

import './Modal.css';

type Props = {
  children: React.ReactNode;
  className?: string;
  visible: boolean;
  backdrop?: boolean;
  onCancel?: () => void;
  cancelButtonText?: string;
  onAccept: () => void;
  acceptButtonText?: string;
  disableAccept?: boolean;
  hideCancelButton?: boolean;
  cancelButtonVariant?: ButtonVariant;
  acceptButtonVariant?: ButtonVariant;
  hideButtons?: boolean;
};

const Modal: FC<Props> = ({
  children,
  className,
  visible,
  backdrop,
  onCancel,
  cancelButtonText = 'modal.cancel',
  onAccept,
  acceptButtonText = 'modal.accept',
  disableAccept = true,
  hideCancelButton = false,
  cancelButtonVariant = ButtonVariant.FLAT,
  acceptButtonVariant = ButtonVariant.SECONDARY,
  hideButtons = false,
}) => {
  const { t } = useTranslation();
  return (
    <div
      className={classNames('modal', {
        'modal--visible': visible,
      })}
    >
      <div className={'modal__container'}>
        <div className="modal__modal-main">{children}</div>
        {!hideButtons && (
          <div className="modal__modal-footer">
            {!hideCancelButton && (
              <Button
                onClick={onCancel}
                text={t(cancelButtonText)}
                variant={cancelButtonVariant}
                dataCy="modal-cancel-button"
              />
            )}
            <Button
              className="modal__modal-footer__accept-button"
              onClick={onAccept}
              text={t(acceptButtonText)}
              variant={acceptButtonVariant}
              disabled={disableAccept}
              dataCy="modal-accept-button"
            />
          </div>
        )}
      </div>
      {backdrop && (
        <div
          className={classNames('modal-backdrop', className, {
            'modal-backdrop--visible': visible,
          })}
        ></div>
      )}
    </div>
  );
};

export default Modal;
