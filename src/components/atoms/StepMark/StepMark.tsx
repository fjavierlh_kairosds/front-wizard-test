import { FC } from 'react';
import './StepMark.css';

const StepMark: FC = () => {
  return <span className="step-mark"></span>;
};

export default StepMark;
