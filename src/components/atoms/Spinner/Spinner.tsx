import './Spinner.css';

const Spinner = () => <span className="spinner"></span>;

export default Spinner;
