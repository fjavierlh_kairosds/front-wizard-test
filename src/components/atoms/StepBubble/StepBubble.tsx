import classNames from 'classnames';
import { FC } from 'react';
import './StepBubble.css';

type Props = {
  stepNumber: number;
  active: boolean;
  success: boolean;
};

const StepBubble: FC<Props> = ({ stepNumber, active, success }) => {
  return (
    <div
      data-cy={`step-bubble-${stepNumber}`}
      key={stepNumber}
      className={classNames('step-bubble', {
        'step-bubble--active': active,
        'step-bubble--success': success,
      })}
    >
      <span>{success ? '✓' : stepNumber}</span>
    </div>
  );
};

export default StepBubble;
