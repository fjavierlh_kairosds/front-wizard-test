import { FC, useState, MouseEvent } from 'react';
import { PasswordSecurityLevels } from './PasswordSecurityLevels';
import './InputPassword.css';
import { useTranslation } from 'react-i18next';

type Props = {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onError: (e: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
  placeholder: string;
  label: string;
  dataCy?: string;
};
const InputPassword: FC<Props> = ({
  label,
  onChange,
  placeholder,
  value,
  dataCy,
}) => {
  const { t } = useTranslation();
  const [showPassword, setshowPassword] = useState(false);
  const [level, setLevel] = useState(PasswordSecurityLevels.LOW);
  const [error, setError] = useState('');

  const handleClick = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setshowPassword(!showPassword);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const regexMedium = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{4,}$/;
    const regexHigh = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,24}$/;

    const newValue = e.target.value;

    const newLevel = regexHigh.test(newValue)
      ? PasswordSecurityLevels.HIGH
      : regexMedium.test(newValue)
      ? PasswordSecurityLevels.MEDIUM
      : PasswordSecurityLevels.LOW;

    setLevel(newLevel);
    if (newLevel === PasswordSecurityLevels.MEDIUM) {
      setError(t('errors.input_password_level_medium'));
    }
    if (newLevel === PasswordSecurityLevels.LOW) {
      setError(t('errors.input_password_level_low'));
    }
    if (newLevel === PasswordSecurityLevels.HIGH || newValue === '') {
      setError('');
    }
    onChange(e);
  };

  return (
    <label className="input-password" data-cy={dataCy}>
      {label}
      <div className="input-password__input-container">
        <input
          value={value}
          onChange={handleChange}
          type={showPassword ? 'text' : 'password'}
          placeholder={placeholder}
        />
        <button onClick={handleClick}>👁</button>
        {value && (
          <meter
            value={level}
            max="3"
            className="input-password__security-checker"
          ></meter>
        )}
      </div>
      {error && <div className="input-password__error">{error}</div>}
    </label>
  );
};

export default InputPassword;
