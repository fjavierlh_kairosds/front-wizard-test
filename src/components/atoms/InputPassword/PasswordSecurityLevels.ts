export enum  PasswordSecurityLevels {
    LOW = 1,
    MEDIUM = 2,
    HIGH = 3,
}