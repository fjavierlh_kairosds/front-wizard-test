import { FC } from 'react';
import './Checkbox.css';

type Props = {
  label: string;
  onChange: () => void;
  dataCy?: string;
};

const Checkbox: FC<Props> = ({ label, onChange, dataCy }) => {
  const handlerOnChange = () => {
    onChange();
  };
  return (
    <label className="checkbox" data-cy={dataCy}>
      {label}
      <input
        type="checkbox"
        className="checkbox__input"
        onChange={handlerOnChange}
      />
    </label>
  );
};

export default Checkbox;
