import classNames from 'classnames';
import { FC } from 'react';
import { ButtonVariant } from './ButtonVariant';
import './Button.css';

type Props = {
  onClick: () => void;
  text: string;
  className?: string;
  disabled?: boolean;
  variant?: ButtonVariant;
  dataCy?: string;
};
const Button: FC<Props> = ({
  onClick,
  text,
  disabled,
  variant,
  className,
  dataCy,
}) => (
  <button
    className={classNames('button', className, {
      [`button--${variant}`]: variant,
    })}
    data-cy={dataCy}
    onClick={onClick}
    disabled={disabled}
  >
    {text}
  </button>
);

export default Button;
