export enum ButtonVariant {
  FLAT = 'flat',
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  FINISH = 'finish',
}
