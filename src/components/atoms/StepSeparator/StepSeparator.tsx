import classNames from 'classnames';
import { FC } from 'react';
import './StepSeparator.css';

type Props = {
  active: boolean;
  success: boolean;
};

const StepSeparator: FC<Props> = ({active, success}) => {
  return (
    <span
      className={classNames('step-separator', {
        'step-separator--active': active,
        'step-separator--success': success,
      })}
    ></span>
  );
};

export default StepSeparator;
