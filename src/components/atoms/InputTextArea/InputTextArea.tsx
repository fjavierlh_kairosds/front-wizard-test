import classNames from 'classnames';
import { FC } from 'react';
import './InputTextArea.css';

type Props = {
  onChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
  value: string;
  placeholder: string;
  label: string;
  maxLength: number;
  info?: string;
  dataCy?: string;
};

const InputTextArea: FC<Props> = ({
  value,
  onChange,
  placeholder,
  label,
  maxLength = 100,
  info,
  dataCy,
}) => {
  return (
    <div className="input-text-area">
      <label htmlFor="text" className="input-text-area__label" >
        <span>{label} </span>
        {info && (
          <span className="input-text-area__info">
            ⓘ<span className="input-text-area__info__text">{info}</span>
          </span>
        )}
      </label>
      <textarea
        className="input-text-area__text-area"
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        maxLength={maxLength}
        rows={3}
        data-cy={dataCy}
      />
      <span
        className={classNames('input-text-area__letter-counter', {
          'input-text-area__letter-counter--error': value.length === maxLength,
        })}
        data-cy={`${dataCy}-counter-span`}
      >
        {value.length}/{maxLength}
      </span>
    </div>
  );
};

export default InputTextArea;
