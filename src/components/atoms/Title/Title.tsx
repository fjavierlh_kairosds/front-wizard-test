import { FC } from 'react';
import './Title.css';

type TitleSize = 1 | 2 | 3 | 4 | 5 | 6;

enum TitleSizeEnum {
  H1 = 'h1',
  H2 = 'h2',
  H3 = 'h3',
  H4 = 'h4',
  H5 = 'h5',
  H6 = 'h6',
}

type Props = {
  text: string;
  size?: TitleSize;
  className?: string;
};

const Title: FC<Props> = ({ text, size = 1 }) => {
  const TitleSized = `h${size}` as TitleSizeEnum;
  return <TitleSized className="title">{text}</TitleSized>;
};

export default Title;
