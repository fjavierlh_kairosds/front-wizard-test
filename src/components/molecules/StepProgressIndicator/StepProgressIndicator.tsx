import { FC } from 'react';
import { UseCurrentProgressResult } from '../../../hooks/useCurrentProgress';
import StepBubble from '../../atoms/StepBubble/StepBubble';
import StepMark from '../../atoms/StepMark/StepMark';
import StepSeparator from '../../atoms/StepSeparator/StepSeparator';
import './StepProgressIndicator.css';

type Props = {
  currentProgress: Record<number, UseCurrentProgressResult>
};

const StepProgressIndicator: FC<Props> = ({ currentProgress}) => {
  const currentProgressKeys = Object.keys(currentProgress);

  return (
    <div className="step-progress-indicator">
      {currentProgressKeys.map((_, index) => (
        <div className="step-progress-indicator__indicator">
          {index !== currentProgressKeys.length - 1 ? (
            <>
              <StepBubble
                stepNumber={index + 1}
                active={currentProgress[index + 1].active}
                success={currentProgress[index + 1].success}
              />
              <StepSeparator
                active={currentProgress[index + 2].active}
                success={currentProgress[index + 1].success}
              />
              {currentProgress[index + 1].active && <StepMark />}
            </>
          ) : (
            <>
              <StepBubble
                stepNumber={index + 1}
                active={currentProgress[index + 1].active}
                success={currentProgress[index + 1].success}
              />
              {currentProgress[index + 1].active && <StepMark />}
            </>
          )}
        </div>
      ))}
    </div>
  );
};

export default StepProgressIndicator;
