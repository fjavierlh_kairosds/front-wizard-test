import { FC } from 'react';
import WizardHeader from '../../organisms/WizardHeader/WizardHeader';
import Modal from '../../../layout/Modal/Modal';
import './Wizard.css';
import { UseCurrentProgressResult } from '../../../hooks/useCurrentProgress';
import { useTranslation } from 'react-i18next';

type Props = {
  children?: JSX.Element | JSX.Element[];
  isOpen: boolean;
  onClose: () => void;
  currentStep: number;
  steps: number;
  onNextStep: () => void;
  wizardSteps: JSX.Element[];
  currentProgress: Record<number, UseCurrentProgressResult>;
  disableNextStep?: boolean;
};

const Wizard: FC<Props> = ({
  isOpen,
  onClose,
  currentStep,
  steps,
  onNextStep,
  wizardSteps,
  disableNextStep,
  currentProgress,
}) => {
  const { t } = useTranslation();
  return (
    <Modal
      className="wizard__wizard-modal"
      visible={isOpen}
      backdrop={true}
      onCancel={onClose}
      onAccept={onNextStep}
      disableAccept={disableNextStep}
      acceptButtonText={t('password_manager_modal.next_step')}
      cancelButtonText={t('password_manager_modal.cancel')}
      hideButtons={currentStep === steps}
    >
      <WizardHeader currentProgress={currentProgress} />
      <div className="wizard__wizard-content">
        {wizardSteps[currentStep - 1]}
      </div>
    </Modal>
  );
};

export default Wizard;
