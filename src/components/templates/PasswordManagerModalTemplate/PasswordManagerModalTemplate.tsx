import { FC } from 'react';
import { UseCurrentProgressResult } from '../../../hooks/useCurrentProgress';
import Wizard from '../Wizard/Wizard';

type Props = {
  visible: boolean;
  onClose: () => void;
  wizardSteps: JSX.Element[];
  currentStep: number;
  onNextStep: () => void;
  disableNextStep: boolean;
  currentProgress: Record<number, UseCurrentProgressResult>;
};

const PasswordManagerModalTemplate: FC<Props> = ({
  visible,
  onClose,
  wizardSteps,
  currentStep,
  onNextStep,
  disableNextStep,
  currentProgress
}) => {
  return (
    <Wizard
      currentProgress={currentProgress}
      isOpen={visible}
      onClose={onClose}
      steps={wizardSteps.length}
      currentStep={currentStep}
      onNextStep={onNextStep}
      wizardSteps={wizardSteps}
      disableNextStep={disableNextStep}
    />
  );
};

export default PasswordManagerModalTemplate;
