import { FC } from 'react';
import { UseCurrentProgressResult } from '../../../hooks/useCurrentProgress';
import StepProgressIndicator from '../../molecules/StepProgressIndicator/StepProgressIndicator';
import './WizardHeader.css';

type Props = {
  currentProgress: Record<number, UseCurrentProgressResult>;
};

const WizardHeader: FC<Props> = ({ currentProgress }) => {
  return (
    <header className="header">
      <StepProgressIndicator currentProgress={currentProgress} />
    </header>
  );
};

export default WizardHeader;
