import { FC } from 'react';
import group from './../../assets/img/group.svg';
import group3 from './../../assets/img/group-3.svg';
import './ProductInformation.css';
import Checkbox from '../../components/atoms/Checkbox/Checkbox';
import { useTranslation } from 'react-i18next';
import Title from '../../components/atoms/Title/Title';

type Props = {
  onAcceptPolicies: () => void;
};

const ProductInformation: FC<Props> = ({ onAcceptPolicies }) => {
  const { t } = useTranslation();
  return (
    <div className="product-information">
      <Title text={t('password_manager_modal.product_information.title_1')} />
      <div className="product-information__container">
        <div className="product-information__group product-information__group-left">
          <img className="image__left" src={group} alt="Placeholder 15px" />
          <p>{t('password_manager_modal.product_information.paragraph_1_1')}</p>
        </div>
        <div className="product-information__group product-information__group-rigth">
          <img className="image__rigth" src={group3} alt="Placeholder 15px" />
          <p>{t('password_manager_modal.product_information.paragraph_1_2')}</p>
        </div>
      </div>

      <h2>{t('password_manager_modal.product_information.title_2')}</h2>
      <p>{t('password_manager_modal.product_information.paragraph_2_1')}</p>
      <h2>{t('password_manager_modal.product_information.title_3')}</h2>
      <p>{t('password_manager_modal.product_information.paragraph_3_1')}</p>

      <Checkbox
        label={t(
          'password_manager_modal.product_information.accept_terms_label'
        )}
        onChange={onAcceptPolicies}
        dataCy="accept-terms-checkbox"
      />
    </div>
  );
};

export default ProductInformation;
