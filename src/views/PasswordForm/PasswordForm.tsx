import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import InputPassword from '../../components/atoms/InputPassword/InputPassword';
import InputTextArea from '../../components/atoms/InputTextArea/InputTextArea';
import useForm from '../../hooks/useForm';
import './PasswordForm.css';

type Props = {
  onPassword: (
    password: string,
    passwordCheck: string,
    passwordError: boolean,
    hintText?: string
  ) => void;
};

const PasswordForm: FC<Props> = ({ onPassword }) => {
  const {
    password,
    setPassword,
    repassword,
    setRepassword,
    passwordError,
    setPasswordError,
    hintText,
    setHintText,
  } = useForm();

  const { t } = useTranslation();
  return (
    <div className="password-form">
      <h1>{t('password_manager_modal.password_form.title_1')}</h1>
      <p>{t('password_manager_modal.password_form.paragraph_1_1')}</p>

      <form className="password-form__form-container">
        <InputPassword
          label={t('password_manager_modal.password_form.password_label')}
          onChange={(e) => {
            setPassword(e.target.value);
            onPassword(e.target.value, repassword, passwordError);
          }}
          onError={(error) => {
            setPasswordError(Boolean(error));
          }}
          value={password}
          placeholder={t(
            'password_manager_modal.password_form.password_placeholder'
          )}
          dataCy="password-input"
        />
        <InputPassword
          label={t('password_manager_modal.password_form.repassword_label')}
          onChange={(e) => {
            setRepassword(e.target.value);
            onPassword(password, e.target.value, passwordError);
          }}
          onError={(error) => {
            setPasswordError(Boolean(error));
          }}
          value={repassword}
          placeholder={t(
            'password_manager_modal.password_form.repassword_placeholder'
          )}
          dataCy="repassword-input"
        />
        {repassword && password !== repassword && (
          <p className="password-form__error">
            {t(
              'password_manager_modal.password_form.pasword_no_match_error_message'
            )}
          </p>
        )}
        <p>{t('password_manager_modal.password_form.paragraph_1_2')}</p>
        <InputTextArea
          onChange={(e) => {
            setHintText(e.target.value);
            onPassword(password, repassword, passwordError, e.target.value);
          }}
          value={hintText}
          placeholder={t(
            'password_manager_modal.password_form.hint_text_placeholder'
          )}
          label={t('password_manager_modal.password_form.hint_text_label')}
          info={t('password_manager_modal.password_form.hint_text_info')}
          maxLength={255}
          dataCy="hint-text-texarea-input"
        />
      </form>
    </div>
  );
};

export default PasswordForm;
