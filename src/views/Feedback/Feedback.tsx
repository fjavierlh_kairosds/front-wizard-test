import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { ButtonVariant } from '../../components/atoms/Button/ButtonVariant';
import Spinner from '../../components/atoms/Spinner/Spinner';
import Modal from '../../layout/Modal/Modal';

import './Feedback.css';

type Props = {
  success: boolean;
  onSuccess: () => void;
  onError: () => void;
};

const Feedback: FC<Props> = ({ success, onSuccess, onError }) => {
  const { t } = useTranslation();
  return (
    <div className="feedback" data-cy="feedback-div">
      {success === undefined ? (
        <Spinner />
      ) : success ? (
        <>
          <h1 className="feedback__success-icon">✓</h1>
          <Modal
            visible={true}
            onAccept={onSuccess}
            acceptButtonText={t('password_manager_modal.feedback.success.accept_button')}
            disableAccept={false}
            hideCancelButton={true}
            acceptButtonVariant={ButtonVariant.FINISH}
          >
            <h1>{t('password_manager_modal.feedback.success.title')}</h1>
            <p>
            {t('password_manager_modal.feedback.success.paragraph')}
            </p>
          </Modal>
        </>
      ) : (
        <>
          <h1 className="feedback__error-icon">✕</h1>
          <Modal
            visible={true}
            onAccept={onError}
            acceptButtonText={t('password_manager_modal.feedback.error.accept_button')}
            hideCancelButton={true}
            disableAccept={false}
            acceptButtonVariant={ButtonVariant.FINISH}
          >
            <h1>{t('password_manager_modal.feedback.error.title')}</h1>
            <p>
            {t('password_manager_modal.feedback.error.paragraph')}
            </p>
          </Modal>
        </>
      )}
    </div>
  );
};

export default Feedback;
