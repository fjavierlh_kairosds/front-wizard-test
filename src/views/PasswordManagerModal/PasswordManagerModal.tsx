import { FC, useState } from 'react';

import useSubmitForm from '../../hooks/useSubmitForm';
import Feedback from '../Feedback/Feedback';
import PasswordForm from '../PasswordForm/PasswordForm';
import ProductInformation from '../ProductInformation/ProductInformation';
import PasswordManagerModalTemplate from '../../components/templates/PasswordManagerModalTemplate/PasswordManagerModalTemplate';
import useForm from '../../hooks/useForm';
import useToggleProductManagerModal from '../../hooks/useProductManagerModal';
import useWizardSteps from '../../hooks/useWizardSteps';
import useCurrentProgress from '../../hooks/useCurrentProgress';

const PasswordManagerModal: FC = () => {
  const { modalVisible, handlerToogleModal } = useToggleProductManagerModal();

  // Step 1
  const [acceptPolicies, setAcceptPolicies] = useState<boolean>(false);
  const handlerAcceptPolicies = () => {
    setAcceptPolicies((acceptPolicies) => !acceptPolicies);
  };
  const Step1 = <ProductInformation onAcceptPolicies={handlerAcceptPolicies} />;

  // Step 2
  const {
    password,
    setPassword,
    repassword,
    setRepassword,
    passwordError,
    setPasswordError,
    hintText,
    setHintText,
  } = useForm();

  const {
    result: { success },
    handlerSubmit,
    handlerResetSubmitFormResult,
  } = useSubmitForm({ password, repassword, hintText });

  const handlerPassword = (
    password: string,
    repassword: string,
    error: boolean,
    hintText?: string
  ) => {
    setPassword(password);
    setRepassword(repassword);
    setPasswordError(error);
    if (hintText) setHintText(hintText);
  };
  const Step2 = <PasswordForm onPassword={handlerPassword} />;

  // Step 3
  const handlerResetWizardState = () => {
    handlerSetCurrentStep(1);
    handlerResetSubmitFormResult();
    setPassword('');
    setRepassword('');
    setPasswordError(false);
    setHintText('');
    setAcceptPolicies(false);
    setDisabledNextStep(true);
  };

  const handlerOnClose = () => {
    handlerResetWizardState();
    handlerToogleModal();
  };

  const Step3 = (
    <Feedback
      success={success}
      onSuccess={handlerOnClose}
      onError={handlerResetWizardState}
    />
  );

  // Wizard
  const wizardSteps = [Step1, Step2, Step3];

  const {
    currentStep,
    disabledNextStep,
    handlerNextStep,
    handlerSetCurrentStep,
    setDisabledNextStep,
  } = useWizardSteps({
    acceptPolicies,
    passwordError,
    password,
    repassword,
    wizardSteps,
    onSumbit: handlerSubmit,
  });

  const currentProgress = useCurrentProgress({
    steps: wizardSteps.length,
    currentStep,
  });

  return (
    <PasswordManagerModalTemplate
      visible={modalVisible}
      onClose={handlerOnClose}
      currentStep={currentStep}
      onNextStep={handlerNextStep}
      wizardSteps={wizardSteps}
      disableNextStep={disabledNextStep}
      currentProgress={currentProgress}
    />
  );
};

export default PasswordManagerModal;
