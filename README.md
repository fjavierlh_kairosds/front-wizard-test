# wizard

In the project directory, you can run:

### `yarn install`

Run this command to install and link workspace dependencies.

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test:e2e`

Launches cypress to run e2e tests.<br>
