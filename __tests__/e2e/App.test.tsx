import { mount } from '@cypress/react';
import App from '../../src/App';

it('renders must create a valid password manager', () => {
  mount(<App />);

  cy.get('[data-cy="password_manager_button"]')
    .should('be.visible')
    .contains('home.password_manager_button')
    .click();

  cy.get('[data-cy="step-bubble-1"]')
    .should('be.visible')
    .should('have.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-2"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-3"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .should('be.disabled');

  cy.get('[data-cy="accept-terms-checkbox"]').should('be.visible').click();

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .should('not.be.disabled')
    .click();

  cy.get('[data-cy="step-bubble-1"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-2"]')
    .should('be.visible')
    .should('have.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-3"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .should('be.disabled');

  cy.get('[data-cy="password-input"]')
    .should('be.visible')
    .click()
    .type('123Patata');

  cy.get('[data-cy="repassword-input"]')
    .should('be.visible')
    .click()
    .type('123Patata');

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .should('be.not.disabled');

  cy.get('[data-cy="hint-text-texarea-input"]')
    .should('be.visible')
    .click()
    .type('a'.repeat(255));

  cy.get('[data-cy="hint-text-texarea-input-counter-span"]')
    .should('be.visible')
    .should('have.class', 'input-text-area__letter-counter--error');

  cy.get('[data-cy="modal-accept-button"]').click();

  cy.get('[data-cy="step-bubble-1"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-2"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-3"]')
    .should('be.visible')
    .should('have.class', 'step-bubble--active');

  cy.get('[data-cy="feedback-div"]').should('be.visible');

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .contains('password_manager_modal.feedback.success.accept_button')
    .click();
});

it('renders must create a invalid password manager', () => {
  mount(<App />);

  cy.get('[data-cy="password_manager_button"]')
    .should('be.visible')
    .contains('home.password_manager_button')
    .click();

  cy.get('[data-cy="step-bubble-1"]')
    .should('be.visible')
    .should('have.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-2"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-3"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .should('be.disabled');

  cy.get('[data-cy="accept-terms-checkbox"]').should('be.visible').click();

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .should('not.be.disabled')
    .click();

  cy.get('[data-cy="step-bubble-1"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-2"]')
    .should('be.visible')
    .should('have.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-3"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .should('be.disabled');

  cy.get('[data-cy="password-input"]')
    .should('be.visible')
    .click()
    .type('pruebaKO123');

  cy.get('[data-cy="repassword-input"]')
    .should('be.visible')
    .click()
    .type('pruebaKO123');

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .should('be.not.disabled');

  cy.get('[data-cy="hint-text-texarea-input"]')
    .should('be.visible')
    .click()
    .type('a'.repeat(255));

  cy.get('[data-cy="hint-text-texarea-input-counter-span"]')
    .should('be.visible')
    .should('have.class', 'input-text-area__letter-counter--error');

  cy.get('[data-cy="modal-accept-button"]').click();

  cy.get('[data-cy="step-bubble-1"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-2"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-3"]')
    .should('be.visible')
    .should('have.class', 'step-bubble--active');

  cy.get('[data-cy="feedback-div"]').should('be.visible');

  cy.get('[data-cy="modal-accept-button"]')
    .should('be.visible')
    .contains('password_manager_modal.feedback.error.accept_button')
    .click();

    cy.get('[data-cy="step-bubble-1"]')
    .should('be.visible')
    .should('have.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-2"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
  cy.get('[data-cy="step-bubble-3"]')
    .should('be.visible')
    .should('have.not.class', 'step-bubble--active');
});

